//BACKGROUND

function randomBackground(){
    let random = Math.floor(Math.random() * 4);

    if(random == 0){
        document.body.style.backgroundColor="#FF71CE";
    }else if(random == 1){
        document.body.style.backgroundColor="#01CDFE";
    }else if(random == 2){
        document.body.style.backgroundColor="#05FFA1";
    }else if(random == 3){
        document.body.style.backgroundColor="#B967FF";
    }
}

const color1 = document.querySelector('.color1');
const color2 = document.querySelector('.color2');
const color3 = document.querySelector('.color3');
const color4 = document.querySelector('.color4');

color1.addEventListener('click', () => {
    document.body.style.backgroundColor="#FF71CE";
});

color2.addEventListener('click', () => {
    document.body.style.backgroundColor="#01CDFE";
});

color3.addEventListener('click', () => {
    document.body.style.backgroundColor="#05FFA1";
});

color4.addEventListener('click', () => {
    document.body.style.backgroundColor="#B967FF";
});

const randomBtn = document.querySelector('.header__menu__background__desplegable__random');

randomBtn.addEventListener('click', () => {
    randomBackground();
});



//RELOJ

function startTime() {
    let today = new Date();
    let hr = today.getHours();
    let min = today.getMinutes();
    min = checkTime(min);
    document.getElementById("clock").innerHTML = hr + ":" + min;
    let time = setTimeout(function(){ startTime() }, 500);
}

function checkTime(i) {
    if (i < 10) {
        i = "0" + i;
    }
    return i;
}

//ACCOUNT DISPLAYS

const accountDiv$$ = document.querySelector('.content__section__account');
const accountContent$$ = document.querySelector('.content__section__account__content');
const accountLink$$ = document.querySelector('.header__menu__account');
const accountExit$$ = document.querySelector('.exit--account');

accountDiv$$.classList.add('hidden');

accountLink$$.addEventListener('click', ()=>{

    accountDiv$$.classList.remove('hidden');

    aboutDiv$$.classList.add('hidden');
    aboutLink$$.classList.remove('selected');
    
    simonSaysDiv$$.classList.add('hidden');
    simonSaysLink$$.classList.remove('selected');

    gamesDiv$$.classList.add('hidden');
    gamesLink$$.classList.remove('selected');

    indexDescription$$.classList.add('hidden');
    
});

accountExit$$.addEventListener('click', ()=>{
    accountDiv$$.classList.add('hidden');
    accountLink$$.classList.remove('selected');

    indexDescription$$.classList.remove('hidden');

    
});

//REGISTER/LOGIN

//REGISTER
/*
function formSubmit(event) {
    var url = "http://localhost:3000/users";
    var request = new XMLHttpRequest();
    request.open('POST', url, true);
    request.onload = function() { // request successful
    // we can use server response to our request now
      console.log(request.responseText);
    };
  
    request.onerror = function() {
      // request failed
    };
  
    request.send(new FormData(event.target)); // create FormData from form that triggered event
    event.preventDefault();
}
  
  // and you can attach form submit event like this for example

const formId = document.getElementById('registerForm');
formId.addEventListener("submit", formSubmit);

*/

/* ESTO ES LO QUE HACÍA QUE NO FUNCIONASE !!!

const registerBtn = document.querySelector('.registerBtn');
const registerEmail = document.querySelector('.registerEmail');
const registerName = document.querySelector('.registerName');

registerBtn.addEventListener('click', () => {
    registerEmail.value = '';
    registerName.value = '';
    userLogEmail.value = '';
    userLogPass.value = '';
});

*/

//LOGIN

let userLog = [];

const logOut$$ = document.querySelector('.logOutFormBtn');

const userLogEmail = document.querySelector('.logInFormEmail');
const userLogPass = document.querySelector('.logInFormPassword');

const userLogBtn = document.querySelector('.logInFormBtn');

userLogBtn.addEventListener('click', () => {
    userLog.push(userLogEmail.value);
    userLog.push(userLogPass.value);

    getUser();
});

async function getUser() {
    const res = await fetch('http://localhost:3000/users');
    const users = await res.json();

    const logEmail = document.querySelector('.logInFormEmail');
    const logPassword = document.querySelector('.logInFormPassword');

    const registerForm$$ = document.querySelector('.registerForm');
    const loginForm$$ = document.querySelector('.logInForm');

    const registerSpan$$ = document.querySelector('.content__section__account__content__register');
    const loginSpan$$ = document.querySelector('.content__section__account__content__login');

    const userName$$ = document.querySelector('.content__section__account__content__userName');

    for(user of users){
        if(user.email == userLog[0] && user.password == userLog[1]){
            registerEmail.value = '';
            registerName.value = '';
            userLogEmail.value = '';
            userLogPass.value = '';
            userLog.splice(0,2);

            logOut$$.classList.remove('hidden');

            userLogBtn.classList.add('hidden');
            registerForm$$.classList.add('hidden');
            loginForm$$.classList.add('hidden');
            registerSpan$$.classList.add('hidden');
            loginSpan$$.classList.add('hidden');

            localStorage.setItem('user', user.name);
            console.log(localStorage);

            userName$$.innerHTML=`
                Welcome, ${user.name}
            `;

            accountLink$$.innerHTML=`
                yourAccount(${user.name})
            `;

        }else if((user.email == userLog[0] && user.password != userLog[1]) || (user.email != userLog[0] && user.password == userLog[1])){
            alert('Error en la identificación');
            registerEmail.value = '';
            registerName.value = '';
            userLogEmail.value = '';
            userLogPass.value = '';
            userLog.splice(0,2);
            break;
        }
    }

    logOut$$.addEventListener('click', () => {
        logOut$$.classList.add('hidden');
        userName$$.classList.add('hidden');

        accountLink$$.innerHTML='your Account';

        userLogBtn.classList.remove('hidden');
        registerForm$$.classList.remove('hidden');
        loginForm$$.classList.remove('hidden');
        registerSpan$$.classList.remove('hidden');
        loginSpan$$.classList.remove('hidden');

        localStorage.clear();
        location.reload();
    });

}

//GAMES DISPLAYS

const gamesDiv$$ = document.querySelector('.content__section__games');
const gamesContent$$ = document.querySelector('.content__section__games__content');
const gamesLink$$ = document.querySelector('.content__nav__item__games');
const gamesExit$$ = document.querySelector('.exit--games');

gamesDiv$$.classList.add('hidden');

gamesLink$$.addEventListener('click', ()=>{

    gamesLink$$.classList.add('selected');
    gamesDiv$$.classList.remove('hidden');

    accountDiv$$.classList.add('hidden');
    accountLink$$.classList.remove('selected');

    aboutDiv$$.classList.add('hidden');
    aboutLink$$.classList.remove('selected');
    
    simonSaysDiv$$.classList.add('hidden');
    simonSaysLink$$.classList.remove('selected');

    indexDescription$$.classList.add('hidden');

    
});

gamesExit$$.addEventListener('click', ()=>{
    gamesDiv$$.classList.add('hidden');
    gamesLink$$.classList.remove('selected');

    indexDescription$$.classList.remove('hidden');

    
});

//GAMES CONTENT

async function getGames () {
    const res = await fetch('http://localhost:3000/games');
    const games = await res.json();

    for(game of games){
        const gameDiv$$ = document.createElement('div');
        gameDiv$$.classList.add('game');
        gameDiv$$.classList.add(game.id);

        gameDiv$$.innerHTML = `
            <div class="game__logo">
                <img src="../db/icons/${game.icon}" alt="logo ${game.name}">
            </div>
            <h4>${game.name}.info</h4>
            <p>Created by: ${game.createdBy}</p>
        `;

        gamesContent$$.appendChild(gameDiv$$);

    }

}

//DETAIL

async function getGameDetail () {
    const res = await fetch('http://localhost:3000/games/');
    const games = await res.json();

    const gameDetailLinks$$ = document.querySelectorAll('.game');

    for(var i = 0; i<gameDetailLinks$$.length; i++){
        const gameDetailLink$$ = gameDetailLinks$$[i];
        gameDetailLink$$.addEventListener('click', () => {
            
            for(game of games){
                if(game.id == gameDetailLink$$.classList.item(1)){
                    const gameDetailDiv$$ = document.createElement('div');
                    gameDetailDiv$$.classList.add('gameDetail');
                    gameDetailDiv$$.classList.add(game.name);

                    const gamesDivHeader$$ = document.querySelector('.content__section__games__header__h3');
                    gamesDivHeader$$.innerHTML = `
                        ${game.name}.info
                    `;

                    const gamesDivContent$$ = document.querySelector('.content__section__games__content');
                    gamesDivContent$$.classList.add('hidden');

                    gameDetailDiv$$.innerHTML = `
                        <div class="gameDetail__image">
                            <img src="../db/images/${game.image}" alt="${game.name}">
                        </div>
                        <div class="gameDetail__info">
                            <h4>${game.name} <span class="gameDetail__info__back">back</span></h4>
                            <p><span>Description:</span> ${game.description}</p>
                            <p><span>Created By:</span> ${game.createdBy}</p>
                            <a href="${game.link}" target="_blank">Go to Repository</a>
                        </div>
                    `;

                    gamesDiv$$.appendChild(gameDetailDiv$$);
                    const backFirst$$ = document.querySelector('.gameDetail__info__back');
                    const backs$$ = document.querySelectorAll('.gameDetail__info__back');

                    function detailExit(){
                        gamesDivHeader$$.innerHTML = 'Games';
                        gamesDivContent$$.classList.remove('hidden');
                        gameDetailDiv$$.classList.add('hidden');
                    }

                    gamesExit$$.addEventListener('click', ()=>{
                        detailExit();
                    });

                    accountLink$$.addEventListener('click', () =>{
                        detailExit();
                    });

                    aboutLink$$.addEventListener('click', ()=>{
                        detailExit();
                    });

                    simonSaysLink$$.addEventListener('click', ()=>{
                        detailExit();
                    });

                    backFirst$$.addEventListener('click', () =>{
                        detailExit();
                        
                    });

                    if(backs$$ != undefined){
                        for(var i=0; i < backs$$.length; i++){
                            let back$$ = backs$$[i];
                            back$$.addEventListener('click', () =>{
                                detailExit();
                                
                            });
                        }
                    }

                }
            }
        });

    }

}

//ABOUT
const indexDescription$$ = document.querySelector('.index__description');

const aboutDiv$$ = document.querySelector('.content__section__about');
const aboutLink$$ = document.querySelector('.content__nav__item__about');
const aboutExit$$ = document.querySelector('.exit--about');

aboutDiv$$.classList.add('hidden');

aboutLink$$.addEventListener('click', ()=>{
    aboutLink$$.classList.add('selected');
    aboutDiv$$.classList.remove('hidden');

    accountDiv$$.classList.add('hidden');
    accountLink$$.classList.remove('selected');
    
    simonSaysDiv$$.classList.add('hidden');
    simonSaysLink$$.classList.remove('selected');

    gamesDiv$$.classList.add('hidden');
    gamesLink$$.classList.remove('selected');

    indexDescription$$.classList.add('hidden');

    
});

aboutExit$$.addEventListener('click', ()=>{
    aboutDiv$$.classList.add('hidden');
    aboutLink$$.classList.remove('selected');

    indexDescription$$.classList.remove('hidden');

    
});

const switch$$ = document.querySelector('.switch');

function switchFx(){
    setInterval(() => {
        switch$$.classList.add('hidden');
    }, 500);

    setInterval(() => {
        switch$$.classList.remove('hidden');
    }, 1000);
} 

//DISPLAYS SIMON SAYS

const simonSaysDiv$$ = document.querySelector('.content__section__simonSays');
const simonSaysLink$$ = document.querySelector('.content__nav__item__simonSays');
const simonSaysExit$$ = document.querySelector('.exit--ss');

simonSaysDiv$$.classList.add('hidden');

simonSaysLink$$.addEventListener('click', ()=>{
    simonSaysLink$$.classList.add('selected');
    simonSaysDiv$$.classList.remove('hidden');

    accountDiv$$.classList.add('hidden');
    accountLink$$.classList.remove('selected');
    
    aboutDiv$$.classList.add('hidden');
    aboutLink$$.classList.remove('selected');

    gamesDiv$$.classList.add('hidden');
    gamesLink$$.classList.remove('selected');

    indexDescription$$.classList.add('hidden');

    
});

simonSaysExit$$.addEventListener('click', ()=>{
    simonSaysDiv$$.classList.add('hidden');
    simonSaysLink$$.classList.remove('selected');

    indexDescription$$.classList.remove('hidden');

    
});



//--------------------------------------------------
//SIMONSAYS

const partes = document.querySelectorAll('.simonSays__square');

const verde = document.querySelector('.ss__green');
const rojo = document.querySelector('.ss__red');
const amarillo = document.querySelector('.ss__yellow');
const azul = document.querySelector('.ss__blue');

const maquina = new Array;
const jugador = new Array;

const score = document.querySelector("#score");
const start = document.querySelector("#start");

function clicar(int_ronda){
	let mas_jugador = jugador.push(int_ronda);
	int_ronda.style.opacity="0.6";

	let tiempo_vis_clicar = setTimeout(function(){
		int_ronda.style.opacity="1";
	}, 500);

	for (let i = 0; i < jugador.length; i++) {

		if(jugador[i] != maquina[i]){
			alert("GAME OVER");
			maquina.length = 0;
			jugador.length = 0;
		}

		if(jugador.length == maquina.length){
			if(jugador[jugador.length-1] != maquina[jugador.length-1]){
				alert("GAME OVER");
				maquina.length = 0;
				jugador.length = 0;
			}else {
			score.innerHTML = "Puntuación: "+maquina.length;
			jugador.length = 0;
			ronda();
			}	
		}

	}
}

function ronda() {
	let res_ronda = partes[Math.floor(Math.random()*partes.length)];
	let mas_maquina = maquina.push(res_ronda);

	for(let rondas_todas = 0; rondas_todas < maquina.length; rondas_todas++){
		let colors_ant = maquina[rondas_todas];

		maquina.forEach( function(valor, indice, array){

			let tiempo1 = 1000*(parseInt(indice)+1);
			let tiempo2 = tiempo1+500;

			let tiempo_sep = setTimeout(function(){
				valor.style.opacity="0.6";
			}, tiempo1);

			let tiempo_vis = setTimeout(function(){
				valor.style.opacity="1";
			}, tiempo2);

		});

	}

}

verde.addEventListener('click', function(){
	clicar(verde);
});
rojo.addEventListener('click', function(){
	clicar(rojo);
});
amarillo.addEventListener('click', function(){
	clicar(amarillo);
});
azul.addEventListener('click', function(){
	clicar(azul);
});

start.addEventListener('click', function(){
		jugador.length = 0;
		maquina.length = 0;
		score.innerHTML = "Puntuación: "+maquina.length;
		ronda();

});

//--------------------------------------------------

window.onload = () => {
    randomBackground();
    getGames();
    getGameDetail ();
    switchFx();
    startTime();
};